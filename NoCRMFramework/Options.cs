﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoCRMFramework
{
    [Verb("pull", HelpText = "Add file contents to the index.")]
    public class PullOptions
    {
        [Option('c', "case", Required = true, HelpText = "Case number to pull")]
        public string CaseId { get; set; }

    }

    [Verb("update", HelpText = "Update a case")]
    public class UpdateOptions
    {
        [Option('c', "case", Required = true, HelpText = "Case number to update")]
        public string CaseId { get; set; }

        [Option('j', "jira", Required = false, HelpText = "Jira Ticket Number")]
        public string JiraTicketNumber { get; set; }

        [Option('o', "open", Required = false, HelpText = "Set case to open")]
        public bool Open { get; set; }

        
    }

    [Verb("addnote", HelpText = "Add a note. Remember to use quotes for multiple words.")]
    public class AddNoteOptions
    {
        [Option('c', "case", Required = true, HelpText = "Case number to update")]
        public string CaseId { get; set; }

        [Option('s', "subject", Required = false, HelpText = "Add a subject to a Developer Note. Use quotes for multiple word strings.")]
        public string Subject { get; set; }

        [Option('b', "body", Required = true, HelpText = "Add a body to a Developer Note. Use quotes for multiple word strings.")]
        public string Body { get; set; }
    }
}
