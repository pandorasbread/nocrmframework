﻿using System;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using Newtonsoft.Json;
using System.IO;

namespace NoCRMFramework
{
    class NoCRM
    {
        static public int Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            return ParseArgs(args).GetAwaiter().GetResult();
        }

        static public System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string dllName = args.Name.Contains(",") ? args.Name.Substring(0, args.Name.IndexOf(',')) : args.Name.Replace(".dll", "");

            dllName = dllName.Replace(".", "_");

            if (dllName.EndsWith("_resources")) return null;

            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("NoCRMFramework" + ".Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());

            byte[] bytes = (byte[])rm.GetObject(dllName);

            return System.Reflection.Assembly.Load(bytes);
        }

        static async public Task<int> ParseArgs(string[] args)
        {
            return await Parser.Default.ParseArguments<PullOptions, UpdateOptions, AddNoteOptions>(args)
                .MapResult(
                (PullOptions opts) => RunPull(opts),
                (UpdateOptions opts) => UpdateCase(opts),
                (AddNoteOptions opts) => AddNote(opts),
                errs => Task.FromResult(1));
        } 

        static public void PrintResponseContent(string message, HttpResponseMessage response, bool writeToHtml = false)
        {
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(message);
                if (!string.IsNullOrEmpty(responseContent))
                {
                    JObject body = JObject.Parse(responseContent);
                    if(writeToHtml)
                    {
                        var fileName = body["value"][0]["tyl_id"] + ".html";
                        var value = body["value"][0];
                        File.WriteAllText(@"./" + fileName, JsonConvert.SerializeObject(value));
                    }
                    var formattedBody = JsonConvert.SerializeObject(body, Formatting.Indented);
                    Console.WriteLine(formattedBody); 
                }
            }
            else
            {
                Console.WriteLine("The request failed with a status of '{0}'",
                       response.ReasonPhrase);
            }
        }

        static async public Task<int> RunPull(PullOptions opts)
        {
            var incidentHelper = new IncidentHelper();
            try
            {
                var response = await incidentHelper.PullIncident(opts.CaseId);
                PrintResponseContent($"Case {opts.CaseId} Details: ", response, true);

            }
            catch (Exception ex)
            {
                DisplayException(ex);
                throw;
            }
            return 0;
        }

        static async public Task<int> UpdateCase(UpdateOptions opts)
        {
            var incidentHelper = new IncidentHelper();
            try
            {
                if(!string.IsNullOrEmpty(opts.JiraTicketNumber))
                { 
                    var response = await incidentHelper.UpdateIncidentTitle(opts.CaseId, opts.JiraTicketNumber);
                    PrintResponseContent($"Case {opts.CaseId} Title Changed ", response);
                }

                if(opts.Open)
                {
                    var response = await incidentHelper.OpenIncident(opts.CaseId);
                    PrintResponseContent($"Case {opts.CaseId} Set to Open ", response);
                }
            }
            catch (Exception ex)
            {
                DisplayException(ex);
                throw;
            }
            return 0;
        }

        static async public Task<int> AddNote(AddNoteOptions opts)
        {
            var incidentHelper = new IncidentHelper();
            try
            {  
                if (!string.IsNullOrEmpty(opts.Body))
                {
                    var subject = string.IsNullOrEmpty(opts.Subject) ?  "Developer Update: " + DateTime.Now.ToShortDateString() : opts.Subject;

                    var response = await incidentHelper.UpdateIncidentActivity(opts.CaseId, opts.Body, subject);
                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Note added successfully");
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayException(ex);
                throw;
            }
            return 0;
        }

        /// <summary> Displays exception information to the console. </summary>
        /// <param name="ex">The exception to output</param>
        private static void DisplayException(Exception ex)
        {
            Console.WriteLine("The application terminated with an error.");
            Console.WriteLine(ex.Message);
            while (ex.InnerException != null)
            {
                Console.WriteLine("\t* {0}", ex.InnerException.Message);
                ex = ex.InnerException;
            }
        }
    }
}

