﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NoCRMFramework
{
    class AuthenticationHandler : DelegatingHandler
    {
        private AuthenticationHeaderValue authHeader;

        public AuthenticationHandler(string serviceUrl, string clientId, string redirectUrl,
                HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
            // Obtain the Azure Active Directory Authentication Library (ADAL) authentication context.
            AuthenticationParameters ap = AuthenticationParameters.CreateFromResourceUrlAsync(
                    new Uri(serviceUrl + "api/data/")).Result;
            AuthenticationContext authContext = new AuthenticationContext("https://login.microsoftonline.com/common");
            AuthenticationResult authResult = GetToken(authContext, serviceUrl, clientId, redirectUrl).GetAwaiter().GetResult();

            authHeader = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
        }

        public static async Task<AuthenticationResult> GetToken(AuthenticationContext authContext, string serviceUrl, string clientId, string redirectUrl)
        {
            var authresult = await authContext.AcquireTokenAsync(serviceUrl, clientId, new Uri(redirectUrl), new PlatformParameters(PromptBehavior.Auto, null));
            return authresult;
        }


        protected override Task<HttpResponseMessage> SendAsync(
                 HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            request.Headers.Authorization = authHeader;
            return base.SendAsync(request, cancellationToken);
        }
    }
}
