﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NoCRMFramework
{
    class IncidentHelper
    {
        private static string serviceUrl = "https://tylertech.crm.dynamics.com/";   // CRM Online
        private static string clientId = "26d031bd-6bde-4df5-bcd5-21751c9ef64b";
        private static string redirectUrl = "https://www.tylertech.com";
        private static HttpClient httpClient; 
        public IncidentHelper()
        {
            httpClient = new HttpClient(new AuthenticationHandler(serviceUrl, clientId, redirectUrl, new HttpClientHandler()));
            httpClient.BaseAddress = new Uri(serviceUrl);
            httpClient.Timeout = new TimeSpan(0, 2, 0);  //2 minutes

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public HttpResponseMessage whoAmI()
        {
            return httpClient.GetAsync("api/data/v9.0/WhoAmI", HttpCompletionOption.ResponseHeadersRead).Result;
        }

        public async Task<HttpResponseMessage> PullIncident(string caseId)
        {
            return await httpClient.GetAsync($"api/data/v9.0/incidents?$select=tyl_id,incidentid,title,tyl_originaltitle,description,createdon,statuscode,prioritycode&$filter=tyl_id eq '{caseId}'&$expand=Incident_ActivityPointers($select=subject,description,createdon,activityid)", HttpCompletionOption.ResponseHeadersRead);
        }

        public async Task<HttpResponseMessage> UpdateIncidentActivity(string caseId, string body, string subject)
        {
            var response = await httpClient.GetAsync($"api/data/v9.0/incidents?$select=tyl_id,incidentid&$filter=tyl_id eq '{caseId}'", HttpCompletionOption.ResponseHeadersRead);
            
            JObject jBod = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var incidentId = (string)jBod["value"][0]["incidentid"];

            JObject activityContent = new JObject();
            activityContent.Add("description", body);
            activityContent.Add("subject", subject);
            activityContent.Add("activitytypecode", "tyl_note");
            activityContent.Add("regardingobjectid_incident@odata.bind", "/incidents(" + incidentId + ")");

            return await httpClient.PostAsJsonAsync($"api/data/v9.0/tyl_notes", activityContent);
        }

        public async Task<HttpResponseMessage> UpdateIncidentTitle(string caseId, string jiraTicketNumber)
        {
            var response = await httpClient.GetAsync($"api/data/v9.0/incidents?$select=tyl_id,incidentid,title&$filter=tyl_id eq '{caseId}'", HttpCompletionOption.ResponseHeadersRead);

            JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            var title = (string)body["value"][0]["title"];

            var value = $"{jiraTicketNumber} {title}";

            JObject postBody = new JObject();
            postBody.Add("value", value);

            var id = (string)body["value"][0]["incidentid"];

            return await httpClient.PutAsJsonAsync($"api/data/v9.0/incidents({id})/title", postBody);
        }

        public async Task<HttpResponseMessage> OpenIncident(string caseId)
        {
            var response = await httpClient.GetAsync($"api/data/v9.0/incidents?$select=tyl_id,incidentid&$filter=tyl_id eq '{caseId}'", HttpCompletionOption.ResponseHeadersRead);

            JObject body = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            JObject postBody = new JObject();
            postBody.Add("value", 2);

            var id = (string)body["value"][0]["incidentid"];

            return await httpClient.PutAsJsonAsync($"api/data/v9.0/incidents({id})/statuscode", postBody);
        }


    }
}